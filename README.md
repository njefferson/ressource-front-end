/ *** heropatterns *** /
https://www.heropatterns.com/


/ *** fonds bg *** /
https://coolbackgrounds.io/

/ *** Loader css **** /
1: https://loading.io/css/
2: https://codepen.io/Negor/pen/pPpVdE/


/ *** bouton animation avec image en fond *** /
https://codepen.io/pizza3/pen/qmerBv


/ *** fichier de balise *** /
https://yaireo.github.io/tagify/


/ *** Bibliothèque JavaScript pour la présentation de différents types de médias (photos, vidéos) *** /
https://fancyapps.com/fancybox/3/

/ *** les onglets s'animent *** /
https://codepen.io/flavio_amaral/pen/OKBxxK


/***Modèle de bouttons menus au responsive***/
https://codepen.io/getignited/pen/rVXWmQ

/*** background animate ***/
1: https://freefrontend.com/css-animated-backgrounds/ 
2: https://www.sliderrevolution.com/resources/css-animated-background/
3: https://bashooka.com/coding/web-background-animation-effects/

/*** active link on scroll ***/
https://codepen.io/BraisC/pen/mxVwVm
/* css */
https://codepen.io/supersarap/pen/EsAyn
/* avec animation */
https://codepen.io/wzielinski/pen/xERdyA

/**** templates portefolio ****/
https://themeforest.net/category/site-templates/creative/portfolio?term=personal%20portfolio
https://preview.themeforest.net/item/elmiz-personal-portfolio-template/full_screen_preview/28321526?_ga=2.38028899.1324541939.1606430902-1095971138.1606430902
http://preview.themeforest.net/item/gfolio-cv-resume/full_screen_preview/27366009?_ga=2.38028899.1324541939.1606430902-1095971138.1606430902


/*** slick carousel ***/
https://kenwheeler.github.io/slick/



/*** Custom input type range ***/
https://rangeslider.js.org/


/** SimpleLightbox (public un peu comme magifique popup mais avec des animation) **/
https://simplelightbox.com/

/** plugin d'effet parallax **/
https://markdalgleish.com/projects/stellar.js/

/** nice select (plugin custom select) **/
https://jqueryniceselect.hernansartorio.com/


/** emoji insert **/
https://codepen.io/g6khan/pen/rqwWVJ

/** slider new style **/
https://codepen.io/vyastech/pen/gzXQJZ


/********Modèle backgrounds****/
https://bggenerator.com/index.php
http://bg.siteorigin.com/
https://bgjar.com/animated-shape.html
https://www.flaticon.com/pattern/
https://wweb.dev/resources/animated-css-background-generator

/****** loader codepen ******/
https://codepen.io/vank0/pen/mARwLg


/***** nice select plugin ********/
https://jqueryniceselect.hernansartorio.com/?utm_source=cdnjs&utm_medium=cdnjs_link&utm_campaign=cdnjs_library


/***** button animate ******/
https://codepen.io/eped22/pen/ZOVJrR
